export function initBurgers() {
  Array.from(
    document.querySelectorAll<HTMLButtonElement>(
      '[data-trigger]'
    )
  ).forEach((button) => {
    const containerId = button.getAttribute('data-container');

    if (null === containerId) return;

    const container = document.querySelector<HTMLElement>(containerId);

    if (null === container) return;
    
    const handler = handleClick(container);
    button.addEventListener('click', handler);
  });
}

type ClickHandler = (event: MouseEvent) => void;
function handleClick(container: HTMLElement): ClickHandler {
  return () => {
    const currentAriaValue = container.getAttribute('aria-hidden');
  
    const normalizedAriaValue = currentAriaValue
      ? currentAriaValue === 'true'
      : true;
    
    container.setAttribute('aria-hidden', (!normalizedAriaValue).toString());
  }
}
