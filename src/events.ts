// Toutes les méthodes pour écouter,
// supprimer et dispatcher des événements
// au sein de votre document HTML

function handleClick(event: MouseEvent): void {}

function createAndListenToEvents() {
  const button = document.querySelector<HTMLButtonElement>('button'); 
  
  button?.addEventListener('click', handleClick);
  button?.removeEventListener('click', handleClick);

  const event = new CustomEvent('carouselprev', { detail: { index: 2 } });
  button?.dispatchEvent(event)

  document?.addEventListener('carouselprev', (event) => {
    console.log(event.detail.index);
  });
}

createAndListenToEvents();

interface HTMLElementEventMap {
  'carouselprev': CustomEvent<{ index: number }>;
}

interface DocumentEventMap {
  'carouselprev': CustomEvent<{ index: number }>;
}